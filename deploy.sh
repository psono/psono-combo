#!/usr/bin/env bash
apk upgrade --no-cache
apk add --update curl skopeo

# Deploy to Docker Hub
skopeo copy --all docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG docker://docker.io/psono/psono-combo:latest

SERVER_VERSION=`docker run psono/psono-combo:latest cat /root/psono/VERSION.txt | sed 's/ .*//'`
CLIENT_VERSION=`docker run psono/psono-combo:latest cat /usr/share/nginx/html/VERSION.txt | sed 's/ .*//'`
PORTAL_VERSION=`docker run psono/psono-combo:latest cat /usr/share/nginx/html/portal/VERSION.txt | sed 's/ .*//'`
skopeo copy --all docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG docker://docker.io/psono/psono-combo:$SERVER_VERSION-$CLIENT_VERSION-$PORTAL_VERSION

echo "Trigger psono pw deployment"
curl -X POST -F token=$PSONO_COMBO_UPDATED_TOKEN -F ref=master https://gitlab.com/api/v4/projects/24874262/trigger/pipeline
