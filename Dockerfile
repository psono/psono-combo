FROM psono/psono-server:latest

LABEL maintainer="Sascha Pfeiffer <sascha.pfeiffer@psono.com>"
COPY cmd.sh /root/configs/docker/cmd.sh
COPY ./templates /root/combo/templates
COPY ./createconfig.py /root/combo/createconfig.py
WORKDIR /root

RUN apk upgrade --no-cache && \
    mkdir -p /root/.pip && \
    apk add --no-cache \
        curl \
        zip \
        nginx && \
    curl https://storage.googleapis.com/get.psono.com/psono/psono-client/latest/webclient.zip --output webclient.zip && \
    mkdir -p webclient && \
    mkdir -p /etc/nginx/conf.d/ && \
    mkdir -p /usr/share/nginx/ && \
    unzip webclient.zip -d webclient && \
    mv webclient /usr/share/nginx/html && \
    rm webclient.zip && \
    curl https://storage.googleapis.com/get.psono.com/psono/psono-admin-client/latest/adminclient.zip --output adminwebclient.zip && \
    mkdir -p adminwebclient && \
    unzip adminwebclient.zip -d adminwebclient && \
    mv adminwebclient /usr/share/nginx/html/portal && \
    rm adminwebclient.zip && \
    apk del --no-cache \
        build-base \
        libffi-dev \
        zip \
        linux-headers && \
    rm -Rf \
        /root/.cache \
        /tmp/*


HEALTHCHECK --interval=2m --timeout=3s \
	CMD curl -f http://localhost/server/healthcheck/ || exit 1

EXPOSE 80

CMD ["/bin/sh", "/root/configs/docker/cmd.sh"]