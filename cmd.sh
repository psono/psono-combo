#!/bin/sh
set -e

if [ ! -z "$PSONO_WEBCLIENT_CONFIG_JSON" ]
then
      echo "$PSONO_WEBCLIENT_CONFIG_JSON" > /usr/share/nginx/html/config.json
fi
if [ ! -z "$PSONO_PORTAL_CONFIG_JSON" ]
then
      echo "$PSONO_PORTAL_CONFIG_JSON" > /usr/share/nginx/html/portal/config.json
fi
if [ ! -z "$PSONO_SERVICE_WORKER_DISABLE" ]
then
      echo "" > /usr/share/nginx/html/js/service-worker-load.js
fi

python3 /root/combo/createconfig.py /root/combo/templates/nginx.conf.tpl /etc/nginx/nginx.conf
python3 /root/combo/createconfig.py /root/combo/templates/default.conf.tpl /etc/nginx/conf.d/default.conf

if [ -z "$PSONO_SKIP_MIGRATIONS" ] || ([ "$PSONO_SKIP_MIGRATIONS" != "True" ] && [ "$PSONO_SKIP_MIGRATIONS" != "true" ] && [ "$PSONO_SKIP_MIGRATIONS" != "TRUE" ]); then python3 /root/psono/manage.py migrate; fi

/bin/sh -c "cd /root/psono && daphne -b 0.0.0.0 -p 8000 asgi:application &" && \
curl --head -X GET --retry 30 --retry-connrefused --retry-delay 1 http://localhost:8000
nginx -g 'daemon off;'
